import React, {Component} from 'react';
import PropTypes from 'prop-types';
import "./modal.scss";
import Button from './Button';

class Modal extends Component{

  render(){
    const {closeButton, header, textModal, changeCondition, addToBasket} = this.props;

    return(
      <>
        <div className='modal'>
            <header className="header">
                <span>{header}</span>
                {closeButton && <div className='cross' onClick={() => changeCondition(0)}>&#10006;</div>}
            </header>
            <main>
                <p className='mainText'>{textModal}</p>
                <div className="ok-cancel">
                    <Button style={{backgroundColor: "rgb(0, 0, 0)", width: "100px", marginRight: "50px"}} textBtn="Ok" onClick={() => addToBasket(0)}/>
                    <Button style={{backgroundColor: "rgb(0, 0, 0)", width: "100px"}} textBtn="Cancel" onClick={() => changeCondition(0)}/>
                </div>
            </main>
        </div>
        <div className="bg_layer" onClick={() => changeCondition(0)}></div>
      </>
    )
  }
}

export default Modal;


Modal.propTypes = {
  closeButton: PropTypes.bool,
  header: PropTypes.string,
  textModal: PropTypes.string,
  changeCondition: PropTypes.func,
  addToBasket: PropTypes.func
}

Modal.defaultProps = {
  closeButton: true,
  header: "???",
  textModal: "???"
}