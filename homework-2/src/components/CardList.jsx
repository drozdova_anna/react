import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Card from './Card';
import "./cardList.scss";

class CardList extends Component{
  showCards = (list, changeModalCondition) => {
    const cardRender = list.map(({name, price, img, code, color}) => {
        return <Card name={name} price={price} img={img} code={code} color={color} key={code} changeModalCondition={changeModalCondition}></Card>
    })
    return cardRender;
  }

  render(){
    const {list, changeModalCondition} = this.props;
    return(
      <div className='cardList'>
        {this.showCards(list, changeModalCondition)}
      </div>
    )
  }
}

export default CardList;


CardList.propTypes = {
  list: PropTypes.array,
  changeModalCondition: PropTypes.func
}

CardList.defaultProps = {
  list: []
}