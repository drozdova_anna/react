import React, {Component} from 'react';
import PropTypes from 'prop-types';
import "./button.scss";

class Button extends Component{

  render(){
    const {textBtn, style, onClick} = this.props;
    return(
      <button type='button' style={style} className='btn' onClick={onClick}>{textBtn}</button>
    )
  }
}

export default Button;

Button.propTypes = {
  textBtn: PropTypes.string,
  style: PropTypes.object,
  onClick: PropTypes.func
}

Button.defaultProps = {
  textBtn: "???",
  style: {backgroundColor: "rgb(0, 0, 0)", width: "100px"}
}