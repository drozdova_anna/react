import React, {Component} from 'react';
import PropTypes from 'prop-types';
import "./card.scss";

class Card extends Component{
  state = {
    isActive: false
  }
  activeStar = (star, key) => {
    this.setState({isActive: star});

    if (star) localStorage.setItem(key, star);
    else      localStorage.removeItem(key);
  }
  ckeckLocalStorage = (key) => {
    const star = localStorage.getItem(key);
    console.log(`${key}: ${star}`);
    //if (star) this.setState({isActive: star});                   ????????????????????????????????????????????????????????????
  }
  render(){
    const {name, price, img, code, color, changeModalCondition} = this.props;
    // if (window.performance.navigation && window.performance.navigation.type === 1) this.ckeckLocalStorage(code);   ?????????????????

    let {isActive} = this.state;
    
    return(
      <div className='card'>
        <h4 className='title'>Pin-up poster: {name}</h4>
        {isActive===false && <div className='star' onClick={() => this.activeStar(true, code)}>&#9734;</div>}
        {isActive===true && <div className='star' onClick={() => this.activeStar(false, code)}>&#9733;</div>}
        <img src={img} alt="poster" />
        <h4 className='price'>Price: {price} &#8364;</h4>
        <div className='card-footer'>
            <p>code: {code}</p>
            <p>color: {color}</p>
        </div>
        <div className='addToBasketBtn' onClick={() => changeModalCondition(true)}>
          Add to basket
        </div>
      </div>
    )
  }
}

export default Card;

Card.propTypes = {
    name: PropTypes.string,
    price: PropTypes.number,
    img: PropTypes.string,
    code: PropTypes.number,
    color: PropTypes.string,
    changeModalCondition: PropTypes.func
}

Card.defaultProps = {
    name: "???",
    price: 0,
    img: "undefined",
    code: 0,
    color: "???"
}