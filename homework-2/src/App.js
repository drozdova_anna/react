import React, {Component} from 'react';
import './App.css';
import Basket from './components/Basket';
import CardList from './components/CardList';
import Modal from './components/Modal';

class App extends Component{
  state = {
    modalCondition : false,
    basketCounter: 0,
    cardList: [],
    basket: [],
    listIsLoaded: false
  }

  addToBasket = (condition) => {
    // const {basket} = this.state;
    // console.log(`Basket in state: ${basket}`);
    // this.setState({basket: basket.push(item)});

    const {basketCounter} = this.state;
    this.setState({basketCounter: basketCounter + 1});

    this.setState({modalCondition: condition});
  }

  changeModalCondition = (condition) => {
    this.setState({modalCondition: condition})
    console.log(condition);
  }

  async componentDidMount() {
    const data = await fetch('API.json')
      .then(response => response.json());
    const {pinup} = data;
    this.setState({
      cardList: pinup,
      listIsLoaded: true
    });

  }
  
  render(){
    const {modalCondition, cardList, listIsLoaded, basketCounter} = this.state;
    
    return(
      <div className='mainWrapper'>
        <header className='shopHeader'>
          <h1 className='shopTitle'>Pin-up posters SHOP</h1>
          <div className='basket'>
            <Basket />
            <p className='basket-counter'>{basketCounter}</p>
          </div>
        </header>
        {listIsLoaded && <CardList list={cardList} addToBasket={this.addToBasket} changeModalCondition={this.changeModalCondition}></CardList>}
        {modalCondition && <Modal changeCondition={this.changeModalCondition} addToBasket={this.addToBasket} closeButton="true" header="Do you want to add this item to Basket?" textModal="After clicking 'OK' the item will be added to your Basket"/>}
      </div>
    )
  }
}

export default App;
