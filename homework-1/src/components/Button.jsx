import React, {Component} from 'react';
import "./button.scss";

class Button extends Component{

  render(){
    const {textBtn, style, onClick} = this.props;
    return(
      <button type='button' style={style} className='btn' onClick={onClick}>{textBtn}</button>
    )
  }
}

export default Button;