import React, {Component} from 'react';
import "./modal.scss";
import Button from './Button';

class Modal extends Component{

  render(){
    const {closeButton, header, textModal, changeCondition} = this.props;

    return(
      <>
        <div className='modal'>
            <header className="header">
                <span>{header}</span>
                {closeButton && <div className='cross' onClick={() => changeCondition(0)}>&#10006;</div>}
            </header>
            <main>
                <p className='mainText'>{textModal}</p>
                <div className="ok-cancel">
                    <Button style={{backgroundColor: "rgb(201, 97, 57)", width: "100px", marginRight: "50px"}} textBtn="Ok" onClick={() => changeCondition(0)}/>
                    <Button style={{backgroundColor: "rgb(201, 97, 57)", width: "100px"}} textBtn="Cancel" onClick={() => changeCondition(0)}/>
                </div>
            </main>
        </div>
        <div className="bg_layer" onClick={() => changeCondition(0)}></div>
      </>
    )
  }
}

export default Modal;