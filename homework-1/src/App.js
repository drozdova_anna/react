import React, {Component} from 'react';
import './App.css';
import Button from "./components/Button";
import Modal from './components/Modal';

class App extends Component{
  state = {
    modalCondition : 0
  }
  changeModalCondition = (condition) => {
    this.setState({modalCondition: condition})
    console.log(condition);
  }

  render(){
    const {modalCondition} = this.state;
    
    return(
      <div className='mainWrapper'>
        <div className='btnWrapper'>
          <Button style={{backgroundColor: 'blue', marginRight: "20px"}} textBtn="Open first modal" onClick={() => this.changeModalCondition(1)}/>
          <Button style={{backgroundColor: 'red'}} textBtn="Open second modal" onClick={() => this.changeModalCondition(2)}/>
        </div>
        {modalCondition===1 && <Modal changeCondition={this.changeModalCondition} closeButton="true" header="Do you want to delete this file" textModal="Once you delete this file, it won't be possible to undo this action"/>}
        {modalCondition===2 && <Modal changeCondition={this.changeModalCondition} closeButton="true" header="Do you want to share this file with your contact" textModal="After clicking 'OK' the file will be sent to your contact by email"/>}
        {/* <Modal changeCondition={this.changeModalCondition} closeButton="true" header="Do you want to delete this file" textModal="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"/> */}
      </div>
    )
  }
}

export default App;
